/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(subject, points, email){
    this.subject = subject;
    this.points = points;
    this.email = email;
  }
  average(){
    let sum = 0
    if(Array.isArray(this.points)){
      for(let i=0;i<this.points.length;i++){
        sum+=this.points[i]
      }
      return sum/this.points.length
    } else {
      return this.points
    }
  }
}

const budi = new Score("matematika", [86, 89, 99], "budi@gmail.com")
const joni = new Score("matematika", 80, "joni@gmail.com")

console.log(budi.average())
console.log(joni.average())

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]


function viewScores(data, subject) {
  // code kamu di sini
  let key = data[0], value;
  let dataObj;
  let state;

  value = data.slice(1, data.length)
  
  for(let i=0;i<data.length-1;i++){
    dataObj = {}
    let item = value[i]
    for(let j=0;j<data[i].length;j++){
      dataObj[key[0]] = item[0]

      switch(subject){
        case "quiz-1":
          state = 1
          break
        case "quiz-2":
          state = 2
          break
        case "quiz-3":
          state = 3
          break
        default:
          state = 1
      }

      dataObj['subject'] = key[state]
      dataObj['points'] = item[state]
    }
  console.log(dataObj)
  }

  return dataObj
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

console.log("\n")

function recapScores(data) {
  // code kamu di sini
  let key = data[0], value;
  let dataObj;
  let sum, avg, title;

  value = data.slice(1, data.length)
  
  for(let i=0;i<data.length-1;i++){
    dataObj = {}
    let item = value[i]
    for(let j=0;j<data[i].length;j++){
      dataObj[key[j]] = item[j]
      sum = item[1] + item[2] + item[3]
      avg = sum/3
    }
    if(avg > 90){
      title = "honour" 
    } else if (avg < 90 && avg > 80){
      title = "graduate"
    } else {
      title = "participant"
    }
    console.log(
    `${1}. Email : ${item[0]}
    Rata-rata : ${avg}
    Predikat : ${title}`)
  }

  return dataObj

}

recapScores(data);
