// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini

function reads(i, time){
    if(i!=books.length){
        readBooks(time, books[i], function(time){
            return reads(i+1, time)
        })
    }
}

reads(0, 10000)
