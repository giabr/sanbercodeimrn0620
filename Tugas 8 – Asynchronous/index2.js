var readBooksPromise = require('./promise.js')
const readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function reads(i, time){
    if(i!=books.length){
        readBooksPromise(time, books[i])
        .then(function(time){
            reads(i+1, time)
        }).catch(function(error){
            console.log(error)
        })
    }
}

reads(0,10000)