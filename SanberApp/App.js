import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/App'
// import RegisterUI from './Tugas/Tugas13/RegisterScreen'
// import LoginUI from './Tugas/Tugas13/LoginScreen'
// import AboutUI from './Tugas/Tugas13/AboutScreen'
// import ToDo from './Tugas/Tugas14/App'
// import SkillScreenUI from './Tugas/Tugas14/SkillScreen'
// import SkillScreenUI from './Tugas/TugasNavigation/SkillScreen'
import Index from './Tugas/Quiz3/index'


export default function App() {
  return (
    // <LoginUI />
    // <RegisterUI />
    // <AboutUI />
    // <ToDo />
    // <SkillScreenUI />
    <Index />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
