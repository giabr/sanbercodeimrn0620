import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'

export default class App extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={{paddingHorizontal: 70, paddingTop: 70, justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={{textAlign:'center', fontSize:24, fontWeight: 'bold', color: '#003366'}}>Tentang Saya</Text>
                    <Icon name='user-circle' color='#efefef' size={150} style={{paddingVertical: 10}} />
                    <Text style={{fontSize: 16, color: '#003366', fontWeight: 'bold'}}>Gia Busyra Rabbani</Text>
                    <Text style={{color: '#3ec6ff', fontWeight: 'bold', paddingVertical: 5}}>React Native Developer</Text>
                </View>
                <View style={styles.iconBox}>
                    <Text style={styles.iconTitle}>Portofolio</Text>
                    <View style={{borderTopColor: '#003366', borderTopWidth: 1, marginTop: 5}}>
                        <View style={styles.iconSubBoxHorizontal}>
                            <TouchableOpacity>
                                <Icon name='gitlab' size={60} style={styles.iconDecoration} />
                                <Text style={styles.iconText}>@giabr</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Icon name='github' size={60} style={styles.iconDecoration} />
                                <Text style={styles.iconText}>@giabr</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.iconBox}>
                    <Text style={styles.iconTitle}>Portofolio</Text>
                    <View style={{borderTopColor: '#003366', borderTopWidth: 1, marginTop: 5, alignItems: 'center'}}>
                        <View style={styles.iconSubBoxVertical}>
                            <TouchableOpacity style={styles.iconVertical}>
                                <Icon name='facebook-square' size={60} style={styles.iconDecorationV} />
                                <Text style={styles.iconText}>Gia Busyra Rabbani</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.iconVertical}>
                                <Icon name='instagram' size={60} style={styles.iconDecorationV} />
                                <Text style={styles.iconText}>@giabusyra</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.iconVertical}>
                                <Icon name='twitter' size={60} style={styles.iconDecorationV} />
                                <Text style={styles.iconText}>@aaburjois</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
          );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 5,
  },
  iconBox: {
    backgroundColor: '#efefef',
    borderRadius: 15,
    marginVertical: 10
  },
  iconSubBoxHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  iconSubBoxVertical: {
    paddingVertical: 20,
  },
  iconTitle: {
    color: '#003366',
    padding: 5
  },
  iconDecoration: {
    color: '#3ec6ff',
  },
  iconDecorationV: {
    color: '#3ec6ff',
    width: 65
  },
  iconVertical: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconText: {
    fontWeight: 'bold',
  }
});
