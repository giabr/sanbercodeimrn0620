import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, Button, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import IconSkill from 'react-native-vector-icons/MaterialCommunityIcons'
import data from '../Tugas14/skillData.json'

function SkillPage({skill}){
    return(
        <TouchableOpacity style={styles.skillPage}>
            <IconSkill name={skill.iconName} size={80} style={{width:80 ,color: '#003366', alignSelf: 'center'}} />
            <View style={{width:125}}>
                <Text style={{color: '#003366', fontWeight: 'bold', fontSize: 16}}>{skill.skillName}</Text>
                <Text style={{color: '#3ec6ff', fontWeight: 'bold', fontSize: 11}}>{skill.categoryName}</Text>
                <Text style={{color: '#ffffff', fontWeight: 'bold', fontSize: 28, textAlign: 'right'}}>{skill.percentageProgress}</Text>
            </View>
            <Icon name='chevron-right' size={40} style={{alignSelf: 'center', color: '#003366'}} />
        </TouchableOpacity>
    )
}

export default class App extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <Image style={styles.logo} source={require('./images/logo.png')} />
                <View style={styles.profile}>
                    <Icon name='user-circle' size={35} style={{paddingRight: 15, color: '#3ec6ff'}} />
                    <View>
                        <Text style={{fontSize: 12}}>Hai,</Text>
                        <Text style={{color: '#003366', fontWeight: 'bold'}}>Gia Busyra Rabbani</Text>
                    </View>
                </View>
                <View style={styles.skillBox}>
                    <Text style={{color: '#003366', fontSize: 24, borderBottomColor: '#3ec6ff', borderBottomWidth: 4}}>SKILL</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <TouchableOpacity style={styles.skillTagBox}>
                            <Text style={styles.skillTag}>Library/Framework</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.skillTagBox}>
                            <Text style={styles.skillTag}>Bahasa Pemrograman</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.skillTagBox}>
                            <Text style={styles.skillTag}>Teknologi</Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView style={{ marginBottom: 220}}>
                    <FlatList 
                        data={data.items}
                        renderItem={(skill)=><SkillPage skill={skill.item} /> }
                        keyExtractor={(item)=>item.id}
                    />
                    </ScrollView>
                </View>
            </View>
          );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
  },
  logo: {
      width: 200,
      resizeMode: 'contain',
      alignSelf: 'flex-end'
  },
  profile: {
      flexDirection: 'row',
      alignItems: 'center'
  },
  skillBox: {
      paddingVertical: 20
  },
  skillTagBox:{
    backgroundColor: '#b4e9ff',
    borderRadius: 10,
    padding: 10,
    marginVertical: 10
  },    
  skillTag: {
      fontSize: 11,
      fontWeight: 'bold',
      color: '#003366'
  },
  skillPage:{
    backgroundColor: '#b4e9ff',
    marginVertical: 10,
    padding: 10,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 3
  }
});
