import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class App extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/logo.png')} />
                </View>
                <View>
                    <Text style={{color: '#003366', textAlign:'center', paddingBottom: 25, fontSize: 16}}>Login</Text>
                    <Text style={styles.itemFillTitle}>Username</Text>
                        <TextInput style={styles.textInput}/>
                    <Text style={styles.itemFillTitle}>Password</Text>
                        <TextInput style={styles.textInput}/>
                </View>
                <View style={{alignItems: 'center', paddingVertical: 20}}>
                    <TouchableOpacity style={styles.buttonMasuk}>
                        <Text style={{color: '#ffffff', fontSize: 18}}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={{color:'#3ec6ff'}}>atau</Text>
                    <TouchableOpacity style={styles.buttonDaftar}>
                        <Text style={{color: '#ffffff', fontSize: 18}}>Daftar ?</Text>
                    </TouchableOpacity>
                </View>
            </View>
          );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 25,
  },
  logo: {
      paddingVertical: 60
  },
  itemFillTitle:{
      color: '#003366',
      fontSize: 12,
      paddingBottom: 5
  },
  textInput:{
      height: 50,
      borderColor: '#003366',
      borderWidth: 1,
      marginBottom: 10,
      padding: 10
  },
  buttonDaftar:{
    borderRadius: 15,
    justifyContent: 'center', 
    backgroundColor: '#003366',
    alignItems: "center", 
    width: 150,
    paddingVertical: 10,
    marginVertical: 15
  },
  buttonMasuk:{
    borderRadius: 15,
    justifyContent: 'center', 
    backgroundColor: '#3ec6ff',
    alignItems: "center", 
    width: 150,
    paddingVertical: 10,
    marginVertical: 15
  }
});
