import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

// import { LoginUI } from './LoginScreen'
// import { AboutUI } from './AboutScreen'
// import { SkillUI } from './SkillScreen'

import {SignIn, CreateAccount, Profile, Home, Search, Details, Search2} from './Screen';

import { StyleSheet, Text, View } from "react-native";
import DrawerLayout from "react-native-gesture-handler/DrawerLayout";

const AuthStack = createStackNavigator();
const Tabs = createStackNavigator()
const HomeStack = createStackNavigator()
const SearchStack = createStackNavigator()

const HomeStack =()=> {
  <HomeStack.Navigator>
    <HomeStack.Screen name='Home' component={Home} />
    <HomeStack.Screen name='Details' component={Details}
     options={(route)=>{
        title: route.params.name
     }} 
    />
  </HomeStack.Navigator>
}

const SearchStack =()=> {
  <SearchStack.Navigator>
    <SearchStack.Screen name='Search' component={Search} />
    <SearchStack.Screen name='Search2' component={Search2} />
  </SearchStack.Navigator>
}

const ProfileStack = createStackNavigator()
const ProfileStack =()=> {
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
}

const TabsScreen =()=> {
  <Tabs.Navigator>
    <Tabs.Screen name='Home' component={HomeStack} />
    <Tabs.Profile name='Profile' component={SearchStack}
  />
</Tabs.Navigator>
}

export default () => {
  <NavigationContainer>
    <Tabs.Navigator>
      <Tabs.Navigator>
        <Tabs.Screen name="Home" component={TabsScreen} />
      </Tabs.Navigator>
      <DrawerLayout.Navigator></DrawerLayout.Navigator>
    </Tabs.Navigator>
    {/* <AuthStack.Navigator>
      <AuthStack.Screen
        name="SignIn"
        component={SignIn}
        options={{title: 'Sign In'}}
      />
      <AuthStack.Screen
        name="CreateAccount"
        component={CreateAccount}
        options={{title: 'Create Account'}}
      />
      <AuthStack.Screen
        name="Skill"
        component={SkillUI}
        options={{title: 'Skill'}}
      />
    </AuthStack.Navigator> */}
  </NavigationContainer>
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
