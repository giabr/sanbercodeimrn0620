// 1
//Release 0

class Animal {
    // Code class di sini
    constructor(
        name,
        legs=4,
        cold_blooded=false
    ){
        this._name = name
        this._legs = legs
        this._cold_blooded = cold_blooded
    }
    get name(){
        return this._name
    }
    get legs(){
        return this._legs
    }
    get cold_blooded(){
        return this._cold_blooded
    }

    set name(x){
        this._name = x
    }
    set legs(x){
        this._legs = x
    }
    set cold_blooded(x){
        this._cold_blooded = x
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\n")

// Release 1

// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name, legs=2, cold_blooded){
        super(name, legs, cold_blooded)
    }
    yell(){
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name, legs, cold_blooded=true){
        super(name, legs, cold_blooded)
    }
    jump(){
        console.log("hop hop")
    }
} 


var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
console.log(sungokong.legs) // 2
console.log(sungokong.cold_blooded) // false
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs) // 4
console.log(kodok.cold_blooded) // true
kodok.jump() // "hop hop" 

console.log("\n")

// 2
// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
//   }
  
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 

class Clock {
    // Code di sini
    constructor({template}){
        this.template = template
    }

    render = () => {
        var date = new Date();
  
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
  
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
  
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
  
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
  
        console.log(output);
    }

    start(){
        this.render()
        this.timer = setInterval(this.render, 1000)
    }

    stop(){
        clearInterval(this.render)
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();