  // soal 1
var peopleObj = {}

function arrayToObject(arr) {
    // Code di sini
    var dateNow = new Date()
    var thisYear = dateNow.getFullYear()

    if(arr.length!=0){
        for(var i=0;i<arr.length;i++){
            peopleObj.firstName = arr[i][0]
            peopleObj.lastName = arr[i][1]
            peopleObj.gender = arr[i][2]
            if(arr[i][3] < thisYear){
                peopleObj.age = thisYear - arr[i][3]
            } else {
                peopleObj.age = "Invalid Birth Year"
            }
    
            console.log(i+1 + ".", peopleObj.firstName, peopleObj.lastName, ":", peopleObj)
        }
    } else {
        console.log('""')
    }
    
    return peopleObj
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("\n")

  // soal 2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var object = {}
    var belanjaan = []
    var sales = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000],
    ]
    var moneyReturn = 0

    sales.sort(function(a,b){
        return b[1] - 1[1]
    })

    if(memberId === undefined || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if(memberId!=null && money < 50000){
        return "Mohon maaf, uang tidak cukup"
    } else {
        for(var i=0;i<sales.length;i++){
            moneyReturn = money
            if(moneyReturn-sales[i][1] >= 0){
                moneyReturn=moneyReturn-sales[i][1]
                belanjaan.push(sales[i][0])
            }
        }
        object.memberId = memberId
        object.money = money
        object.listPurchased = belanjaan
        object.changeMoney = moneyReturn
        return object
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  console.log("\n")

  // soal 3
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var result = []
    var k = 2000

    //your code here
    for(var i=0;i<arrPenumpang.length;i++){
        var awal = arrPenumpang[i][1].charCodeAt(0)
        var akhir = arrPenumpang[i][2].charCodeAt(0)
        var penumpangObj = {}
        var ongkos = (akhir - awal)*k

        penumpangObj.penumpang = arrPenumpang[i][0]
        penumpangObj.naikDari = arrPenumpang[i][1]
        penumpangObj.tujuan = arrPenumpang[i][2]
        penumpangObj.bayar = ongkos
        
        result.push(penumpangObj)
    }
    return result    
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]