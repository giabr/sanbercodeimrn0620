// while
var a = 0;
var b = 2;
var c = 22;
console.log("LOOPING PERTAMA")
while(a < 20){
    a+=b;
    console.log(a + " - I love coding")
}
console.log("LOOPING Kedua")
while(c > 2){
    c-=b;
    console.log(c + " - I will become a mobile developer")    
}

console.log("\n")

// for
for(var i=1; i<=20; i++){
    if(i%2 == 1 && i%3 == 0){
        console.log(i + " - I Love Coding");
    } else if(i%2 == 0){
        console.log(i + " - Berkualitas");
    } else {
        console.log(i + " - Santai");
    }
}

console.log("\n")

// persegi panjang
var persegi_panjang = '';
for(var i=0; i<4; i++){
    for(var j=0; j<8; j++){
        persegi_panjang+='#';
    }
    persegi_panjang+='\n'
}
console.log(persegi_panjang)

// tangga
var tangga = '';
for(var i=0; i<7; i++){
    for(var j=0; j<=i; j++){
        tangga+='#';
    }
    tangga+='\n'
}
console.log(tangga)

// catur
var catur = '';
for(var i=0; i<8; i++){
    if(i%2 == 0){
        for(var j=0; j<8; j++){
            if(j%2 == 0){
                catur+=' ';
            } else {
                catur+='#';
            }
        }
    } else {
        for(var j=0; j<8; j++){
            if(j%2 == 0){
                catur+='#';
            } else {
                catur+=' ';
            }
        }
    }
    catur+='\n'
    }
console.log(catur)
