// 1
var nama = ""
var peran = ""

if(nama == "" && peran == ""){
    console.log("Nama harus diisi!")
} else if(nama == "Jane" && peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if(nama == 'Jenita' && peran== 'Guard'){
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}  else {
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}
console.log("\n")

//2
var hari = 19; 
var bulan = 11; 
var tahun = 1998;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

switch(bulan){
    case 1: {
        bulan = "Januari";
        break;
    }
    case 2: {
        bulan = "Februari";
        break;
    }
    case 3: {
        bulan = "Maret";
        break;
    }
    case 4: {
        bulan = "April";
        break;
    }
    case 5: {
        bulan = "Mei";
        break;
    }
    case 6: {
        bulan = "Juni";
        break;
    }
    case 7: {
        bulan = "Juli";
        break;
    }
    case 8: {
        bulan = "Agustus";
        break;
    }
    case 9: {
        bulan = "September";
        break;
    }
    case 10: {
        bulan = "Oktober";
        break;
    }
    case 11: {
        bulan = "November";
        break;
    }
    case 12: {
        bulan = "Desember";
        break;
    }    
}

tanggal = [hari,bulan,tahun];
console.log(tanggal.join(" "))