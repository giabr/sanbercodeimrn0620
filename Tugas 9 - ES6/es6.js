// 1

// const golden = function goldenFunction(){
//     console.log("This is golden!!")
// }

var golden = () => {
    console.log("This is golden!!")
}

golden()

// 2

// const newFunction = function literal(firstName, lastName){
//     return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function(){
//             console.log(firstName + " " + lastName)
//             return
//         }
//     }
// }

const newFunction =(firstName, lastName)=> {
    return {
        firstName,
        lastName,
        fullName: function() {
            console.log(`${firstName} ${lastName}`);
        }
    }
} 

newFunction("William", "Imoh").fullName()

// 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const {firstName, lastName, destination, occupation, spell} = newObject; 

console.log(firstName, lastName, destination, occupation, spell)

// 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// 5
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before)
