// 1
var numRange = []

function range(startNum, finishNum){
    if(startNum > finishNum){
        for(var i=startNum;i>=finishNum;i--){
            numRange.push(i)
        }
    } else if(startNum < finishNum){
        for(var i=startNum;i<=finishNum;i++){
            numRange.push(i)
        }
    } else {
        numRange.push(-1)
    }
    return numRange
}
console.log(range())
console.log("\n")

// 2
var numRangeStep = []

function rangeWithStep(startNum, finishNum, step) {
    if(startNum > finishNum){
        for(var i=startNum;i>=finishNum;i-=step){
            numRangeStep.push(i)
        }
    } else if(startNum < finishNum){
        for(var i=startNum;i<=finishNum;i+=step){
            numRangeStep.push(i)
        }
    } else {
        numRangeStep = null
    }
    return numRangeStep
}
console.log(rangeWithStep(0,20,2))
console.log("\n")

//3
var numSum = 0

function sum(startNum, finishNum, step) {
    if(step==null){
        step = 1;
    }
    if(startNum > finishNum){
        for(var i=startNum;i>=finishNum;i-=step){
            numSum += i;
        }
    } else if(startNum < finishNum){
        for(var i=startNum;i<=finishNum;i+=step){
            numSum += i;
        }
    } else if(startNum == null && finishNum == null) {
        numSum=0
    } else {
        numSum=startNum
    }
    return numSum
}
console.log(sum())
console.log("\n")

// 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

var preText = [
    "Nomor ID: ", "Nama Lengkap: ", "TTL: ", "Hobi: "
]

var data = []
var fullText = ""

function dataHandling(preText, input){
    for(var i=0; i<input.length; i++){
        for(var j=0; j<input.length;j++){
            var temp = preText[j] + input[i][j] + '\n'
            fullText += temp
        }
        fullText += "\n"
    }
    return fullText
}
console.log(dataHandling(preText, input))
console.log("\n")

// 5
function balikKata(kata){
    var temp = ""
    for(var i=kata.length-1;i>=0;i--){
        temp += kata[i]
    }
    return temp
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("\n")

// 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input){
    input.pop()
    input.splice(4, 0, "Pria", "SMA Internasional Metro")
    input.splice(1,1, "Roman Alamsyah Elsharawy")
    input.splice(2,1, "Provinsi Bandar Lampung")

    // new input after pop and splice
    console.log(input)

    // split date
    var date = input[3].split("/")

    // get month
    var bulan = parseInt(date[1])

    switch(bulan){
        case 1: {
            bulan = "Januari";
            break;
        }
        case 2: {
            bulan = "Februari";
            break;
        }
        case 3: {
            bulan = "Maret";
            break;
        }
        case 4: {
            bulan = "April";
            break;
        }
        case 5: {
            bulan = "Mei";
            break;
        }
        case 6: {
            bulan = "Juni";
            break;
        }
        case 7: {
            bulan = "Juli";
            break;
        }
        case 8: {
            bulan = "Agustus";
            break;
        }
        case 9: {
            bulan = "September";
            break;
        }
        case 10: {
            bulan = "Oktober";
            break;
        }
        case 11: {
            bulan = "November";
            break;
        }
        case 12: {
            bulan = "Desember";
            break;
        }    
    }
    console.log(bulan)

    // sort date descending
    var sortDate = date.slice()
    sortDate.sort(function (a,b){return b-a})
    console.log(sortDate)

    // join date lement
    var newDate = date.join("-")
    console.log(newDate)

    // limit name 15
    var name = input[1].slice(0,15)
    console.log(name)

    return ""
}
 
console.log(dataHandling2(input))

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
